﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameControl : MonoBehaviour
{
    public enum Elemet
    {
        SnakeTail,
        Food,
        Stone,
        Will
    }

    static public List<GameObject> Elements;
    static public List<GameObject> Tails;
    static public List<GameObject> Foods;
    static public List<GameObject> Stones;
    static public List<GameObject> Walls;

    [SerializeField] private GameObject PrefabMainSnake;
    [SerializeField] private Transform TransformSpawnSnake;

    [SerializeField] private Transform WallHeightX;
    [SerializeField] private Transform WallWidthZ;

    [SerializeField] private GameObject FoodPrefab;
    [SerializeField] private GameObject StonePrefab;

    [SerializeField] private UI UIController;

    static public bool IsGame = false;
    [SerializeField] private bool IsWinning = false;
    [SerializeField] private int CountBodySnakeForWinning = 10;

    [SerializeField] private int RandomStone;
    [SerializeField] private int MinRandomStone = 3;
    [SerializeField] private int MaxRandomStone = 5;

    static public UnityAction<bool> EventFinishGame;

    // Start is called before the first frame update
    private void Start()
    {
        Elements = new List<GameObject>();
        Tails = new List<GameObject>();
        Foods = new List<GameObject>();
        Stones = new List<GameObject>();
        Walls = new List<GameObject>();

        Walls.AddRange(GameObject.FindGameObjectsWithTag("Wall"));

        UI.EventStartGame += StartingCreate;
        Snake.EventSnakeHeadTouch += TouchMainSnake;
    }

    private void StartingCreate()
    {
        IsGame = true;

        GameObject Snake = Instantiate(PrefabMainSnake, TransformSpawnSnake.position, Quaternion.identity);
        Elements.Add(Snake);

        UIController.ButtonAssignment(Snake.GetComponent<SnakeController>());

        CreateFood();

        RandomStone = Random.Range(MinRandomStone, MaxRandomStone);

        for (int i = 0; i < RandomStone; i++)
            CreateStone();
    }

    private void CreateFood()
    {
        GameObject Food = Instantiate(FoodPrefab, RandomSpawnElement(), Quaternion.identity);
        Foods.Add(Food);
        Elements.Add(Food);
    }

    private void CreateStone()
    {
        GameObject Stone = Instantiate(StonePrefab, RandomSpawnElement(), Quaternion.identity);
        Stones.Add(Stone);
        Elements.Add(Stone);
    }


    private void FinishGame(bool IsWinning)
    {
        IsGame = false;

        EventFinishGame?.Invoke(IsWinning);
    }

    private void TouchMainSnake(Elemet elemet)
    {
        switch (elemet)
        {
            case Elemet.Food:
            {
                int Width = 0;
                int Height = 0;

                for (int id = 0; id < Walls.Count; id++)
                {
                    if (Walls[id].transform.rotation.y < 0)
                        Height = (int)Walls[id].transform.localScale.x;
                    else
                        Width = (int)Walls[id].transform.localScale.x;
                }

                if ((Elements.Count >= (Width * Height)) || (CountBodySnakeForWinning <= Tails.Count + 1))
                    FinishGame(IsWinning = true);
                else
                    CreateFood();

                break;
            }
            default:
            {
                FinishGame(IsWinning = false);
                break;
            }
        }
    }

    private Vector3 RandomSpawnElement()
    {
        BadPosition:
        int TempX = (int)Random.Range(WallHeightX.position.x * (-1), WallHeightX.position.x);
        int TempZ = (int)Random.Range(WallWidthZ.position.z * (-1), WallWidthZ.position.z);

        Vector3 NewPosition = new Vector3(TempX, 1f, TempZ);

        for(int i = 0; i < Elements.Count; i++)
        {
            if(Elements[i].transform.position == NewPosition)
                goto BadPosition;
        }

        return NewPosition;
    }
}
