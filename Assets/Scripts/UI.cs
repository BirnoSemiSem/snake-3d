﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private GameObject MenuUI;
    [SerializeField] private GameObject GameUI;

    [SerializeField] private Text Titls;

    [SerializeField] private Button _buttonLeft;
    [SerializeField] private Button _buttonRight;

    [SerializeField] private Camera MainCamera;
    [SerializeField] private float CameraFovConst;
    [SerializeField] private Slider _slider;


    static public UnityAction EventStartGame;

    private void Start()
    {
        CameraFovConst = MainCamera.fieldOfView;
        GameControl.EventFinishGame += FinishGame;
    }

    public void ButtonAssignment(SnakeController controller)
    {
        _buttonLeft.onClick.AddListener(controller.LeftButtonOnClick);
        _buttonRight.onClick.AddListener(controller.RightButtonOnClick);
    }

    public void StartGame()
    {
        GameUI.SetActive(true);
        MenuUI.SetActive(false);

        EventStartGame?.Invoke();
    }

    private void FinishGame(bool IsWinning)
    {
        GameUI.SetActive(false);
        MenuUI.SetActive(true);

        if (IsWinning)
            Titls.text = "Ты выиграл!";
        else
            Titls.text = "Ты проиграл!";
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ChangeFovCamera()
    {
        MainCamera.fieldOfView = CameraFovConst * (_slider.value + 1);
    }
}
