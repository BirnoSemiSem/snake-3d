﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeController : MonoBehaviour
{
    [SerializeField] private float Angle = 90f;

    public void LeftButtonOnClick()
    {
        transform.Rotate(new Vector3(0.0f, -Angle, 0.0f));
    }

    public void RightButtonOnClick()
    {
        transform.Rotate(new Vector3(0.0f, Angle, 0.0f));
    }
}
