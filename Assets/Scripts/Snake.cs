﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Snake : MonoBehaviour
{
    [SerializeField] private GameObject Tail;

    private Coroutine CorMoveSnake;
    [SerializeField] private Transform _transform;

    [SerializeField] private float _speed = 0.5f;
    [SerializeField] private int CountStartTails = 1;

    static public UnityAction<GameControl.Elemet> EventSnakeHeadTouch;
    // Start is called before the first frame update
    void Start()
    {
        if (_transform == null)
            _transform = GetComponent<Transform>();

        StartGame();

        GameControl.EventFinishGame += FinishGame;
    }


    private void StartGame()
    {
        for (int i = 0; i < CountStartTails; i++)
        {
            GameObject StartTails = Instantiate(Tail, transform.position, Quaternion.identity);

            GameControl.Elements.Add(StartTails);
            GameControl.Tails.Add(StartTails);
        }

        CorMoveSnake = StartCoroutine(MoveSnake());
    }

    private void FinishGame(bool IsWinning)
    {
        for (int i = 0; i < GameControl.Elements.Count; i++)
            if (GameControl.Elements[i] != null)
                Destroy(GameControl.Elements[i]);

        GameControl.Elements.Clear();
        GameControl.Tails.Clear();
        GameControl.Foods.Clear();
        GameControl.Stones.Clear();

        if (CorMoveSnake != null)
            StopCoroutine(CorMoveSnake);

        CorMoveSnake = null;
    }

    IEnumerator MoveSnake()
    {
        Vector3 PreviousPosition;

        while (GameControl.IsGame)
        {
            PreviousPosition = _transform.position;

            for (int i = 0; i < GameControl.Tails.Count; i++)
            {
                Vector3 temp = GameControl.Tails[i].transform.position;
                GameControl.Tails[i].transform.position = PreviousPosition;
                PreviousPosition = temp;
            }

            transform.position += transform.forward;

            TouchDetected();
            yield return new WaitForSeconds(_speed);
        }

        yield return null;
    }

    private void TouchDetected()
    {
        if (!GameControl.IsGame)
            return;

        int i;

        // Узнаем касается ли змейка своего хвоста | Find out if the snake touches its tail
        for (i = 0; i < GameControl.Tails.Count; i++)
        {
            if (_transform.position == GameControl.Tails[i].transform.position)
            {
                Debug.Log("Коснулся хвоста");

                EventSnakeHeadTouch?.Invoke(GameControl.Elemet.SnakeTail);

                return;
            }
        }

        // Узнаем касается ли змейка еды | Find out if the snake touches the foods
        for (i = 0; i < GameControl.Foods.Count; i++)
        {
            if (_transform.position == GameControl.Foods[i].transform.position)
            {
                Debug.Log("Коснулся еды");

                Destroy(GameControl.Foods[i]);

                GameControl.Elements.Remove(GameControl.Foods[i]);
                GameControl.Foods.Remove(GameControl.Foods[i]);

                if (_speed > 0.2f)
                    _speed -= 0.05f;

                GameObject NewTail = Instantiate(Tail, GameControl.Tails[GameControl.Tails.Count - 1].transform.position, Quaternion.identity);

                GameControl.Elements.Add(NewTail);
                GameControl.Tails.Add(NewTail);

                EventSnakeHeadTouch?.Invoke(GameControl.Elemet.Food);

                return;
            }
        }

        // Узнаем касается ли змейка камня | Find out if the snake touches the stones
        for (i = 0; i < GameControl.Stones.Count; i++)
        {
            if (_transform.position == GameControl.Stones[i].transform.position)
            {
                Debug.Log("Коснулся каменя");

                EventSnakeHeadTouch?.Invoke(GameControl.Elemet.Stone);

                return;
            }
        }

        // Узнаем касается ли змейка стен или вышла за границу | Find out if the snake touches the walls or went beyond the border
        for (i = 0; i < GameControl.Walls.Count; i++)
        {
            float ZonesHeadSnake = 0;
            float ZonesWills = 0;

            if (GameControl.Walls[i].transform.rotation.y < 0)
            {
                ZonesHeadSnake = _transform.position.x < 0 ? -_transform.position.x : _transform.position.x;
                ZonesWills = GameControl.Walls[i].transform.position.x < 0 ? -GameControl.Walls[i].transform.position.x : GameControl.Walls[i].transform.position.x;
            }
            else
            {
                ZonesHeadSnake = _transform.position.z < 0 ? -_transform.position.z : _transform.position.z;
                ZonesWills = GameControl.Walls[i].transform.position.z < 0 ? -GameControl.Walls[i].transform.position.z : GameControl.Walls[i].transform.position.z;
            }

            if (ZonesHeadSnake >= ZonesWills)
            {
                Debug.Log($"Коснулся стены/Вышел за границу");

                EventSnakeHeadTouch?.Invoke(GameControl.Elemet.Will);

                return;
            }
        }
    }
}
